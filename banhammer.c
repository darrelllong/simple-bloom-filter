# include <stdlib.h>
# include <unistd.h>
# include <stdio.h>
# include <string.h>
# include <errno.h>
# include <ctype.h>
# include "hash.h"
# include "bf.h"
# include "ll.h"

# define EMPTY	""

# define MAXWORD       100
# define HASHSZ	     10000
# define BF	 (1 << 20)

# define VERBOSE(x)	{ if (verbose) printf("%s", x); }

# ifndef PREFIX
# define PREFIX "/afs/cats.ucsc.edu/users/g/darrell/"
# endif

# define BADSPEAK PREFIX "/badspeak.txt"
# define NEWSPEAK PREFIX "/newspeak.txt"
# define ESPERANTO PREFIX "/Esperanto.txt"

bool moveToFront  = MTF; // Default value for the Move-to-Front rule

uint32_t distance = 0, // Total seek distance
         count    = 0, // Total number of seeks
         words    = 0, // Number of words in the dictionary
         trans    = 0, // Number of translations
         text     = 0; // Number of words in the scanned text

extern int   yylex(); // Fetches a word
extern char *yytext;  // Text of the word

static inline void makeLowercase(char *word)
{
	for (int i = 0; word[i] != '\0'; i += 1) { word[i] = tolower(word[i]); }
}

void loadDictionary(hashTable *h, bloomF *a, bloomF *b, char *dictionary)
{
	char word[MAXWORD];
	FILE *f = fopen(dictionary, "r");
	if (f == NULL)
	{
		perror("loadDictionary");
		exit(errno);
	}
	while (fscanf(f, "%s", word) != EOF)
	{
		makeLowercase(word);
		words += 1;

		setBF(a, word); setBF(b, word); // Load Bloom filters
		insertHT(h, word, EMPTY); // Add empty translation to table
	}
	fclose(f);
	return;
}

void loadTranslations(hashTable *h, bloomF *a, bloomF *b, char *replacements)
{
	char word[MAXWORD], tran[MAXWORD];
	FILE *f = fopen(replacements, "r");
	if (f == NULL)
	{
		perror("loadTranslations");
		exit(errno);
	}
	while (fscanf(f, "%s %s", word, tran) != EOF)
	{
		makeLowercase(word);
		makeLowercase(tran);
		trans += 1;

		setBF(a, word); setBF(b, word); // Add to Bloom filter

		listNode *t = findHT(h, word);

		if (t)
		{
			free(t->tran);
			t->tran = strdup(tran);		// Replace
		}
		else { insertHT(h, word, tran); }	// New translation
	}
	fclose(f);
	return;
}

int main(int argc, char **argv)
{
	bool stats      = false; // Print statistics?
	bool verbose    = false; // Be verbose?
	bool joyCamp    = false; // Reeducation?
	bool esperanto  = false; // Add Esperanto?
	bool mustRevise = false; // Require revision?

	uint32_t hashSz = HASHSZ; // Default hash table size
	uint32_t BFSz   = BF;	  // Default Bloom filter size

	int c;
        while ((c = getopt(argc, argv, "-emvbsh:f:")) != -1)
	{
		switch (c)
		{
		case 'e': { esperanto   = true;  break; }
		case 'm': { moveToFront = true;  break; }
		case 'b': { moveToFront = false; break; }
		case 'v': { verbose     = true;  break; }
		case 's': { stats       = true; distance = count = 0; break; }
		case 'h': { hashSz      = atoi(optarg); break; }
		case 'f': { BFSz        = atoi(optarg); break; }
		}
	}

	uint32_t initA[] = {0xDeadD00d, 0xFadedBee, 0xBadAb0de, 0xC0c0aB0a}; // First Bloom filter

        bloomF *a = newBF(BFSz, initA);

	uint32_t initB[] = {0xDeadBeef, 0xFadedB0a, 0xCafeD00d, 0xC0c0aB0a}; // Second Bloom filter

	bloomF *b = newBF(BFSz, initB);

	uint32_t initH[] = {0xDeadD00d, 0xFadedBee, 0xBadAb0de, 0xC0c0Babe}; // Hash table

	hashTable *h = newHT(hashSz, initH);

	VERBOSE("Loading dictionary...");
	loadDictionary(h, a, b, BADSPEAK);
	VERBOSE("done.\n");

	VERBOSE("Loading translations...");
	loadTranslations(h, a, b, NEWSPEAK);
	VERBOSE("done.\n");

	if (esperanto)
	{
		VERBOSE("Loading Esperanto...");
		loadTranslations(h, a, b, ESPERANTO);
		VERBOSE("done.\n");
	}

	listNode *revList = NIL; // Words that must be revised
	listNode *badWord = NIL; // Words that are forbidden

	while (yylex() != -1)
	{
		char s[MAXWORD];

		strncpy(s, yytext, MAXWORD);

		for (int i = 0; s[i] != '\0'; i += 1) { s[i] = tolower(s[i]); }

		text += 1; // Count words in the text

		if (memBF(a, s) && memBF(b, s))
		{
			listNode *t = findHT(h, s);
			if (t)
			{
				if (strlen(t->tran) == 0)
				{
				badWord = insertLL(&badWord, s, EMPTY); joyCamp = true;
				}
				else
				{
					 revList = insertLL(&revList, t->key, t->tran); mustRevise = true;
				}
			}
		}
	}

	if (!stats && !joyCamp && mustRevise)
	{
		printf("Dear Comrade,\n\n");
		printf("Submitting your text helps to preserve feelings and prevent\n");
		printf("badthink. Some of the words that you used are not goodspeak.\n");
		printf("The list shows how to turn the oldspeak words into newspeak.\n\n");
		printLL(revList);
	}

	if (!stats && joyCamp)
	{
		printf("Dear Comrade,\n\n");
		printf("You have chosen to use degenerate words that may cause hurt\n");
		printf("feelings or cause your comrades to think unpleasant thoughts.\n");
		printf("This is doubleplus bad. To correct your wrongthink and\n");
		printf("save community consensus we will be sending you to joycamp\n");
		printf("administered by Miniluv.\n");
		printf("\nYour errors:\n\n");
		printLL(badWord);
		if (revList)
		{
			printf("\nThink on these words during your vacation!\n\n");
			printLL(revList);
		}
	}

	if (stats && count > 0)
	{
		printf("Seeks %u, Average %lf, ", distance, (double) distance / (double) count);
		printf("Dictionary %u, Translations %u, Text %u, ", words, trans, text);
		printf("Densities: %lf, %lf\n", countBF(a)/(double) lenBF(a), countBF(b)/(double) lenBF(b));
	}

	delBF(a); delBF(b); delHT(h); delLL(revList); delLL(badWord);
}
