OBJS=aes.o hash.o bf.o ll.o banhammer.o lex.yy.o
CFLAGS=-Wall -Wextra -Wpedantic -g -DMTF=false -DPREFIX=\"./Text\"
CC=gcc

banhammer	:	$(OBJS) banhammer.c bf.h ll.h hash.h
	$(CC) -o banhammer $(OBJS) -ll

lex.yy.c:	words.l
	flex words.l

clean	:
	rm -f banhammer $(OBJS) lex.yy.c
