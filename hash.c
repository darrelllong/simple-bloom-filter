# include <stdlib.h>
# include <stdint.h>
# include <string.h>
# include <stdio.h>
# include "aes.h"
# include "hash.h"

static inline int realLength(int l)
{
	return 16 * (l / 16 + (l % 16 ? 1 : 0));
}

uint32_t hash(hashTable *h, const char *key)
{
	uint32_t output[4] = { 0x0 };
	uint32_t sum       = 0x0;
	int keyL           = strlen(key);
	uint8_t *realKey   = (uint8_t *) calloc(realLength(keyL), sizeof(uint8_t));

	memcpy(realKey, key, keyL);

	for (int i = 0; i < realLength(keyL); i += 16)
	{
		AES128_ECB_encrypt((uint8_t *) h->s, 	        // Salt
                                   (uint8_t *) realKey + i,	// Input
				   (uint8_t *) output);	        // Output
		sum ^= output[0] ^ output[1] ^ output[2] ^ output[3];
	}
	free(realKey);
	return sum;
}

hashTable *newHT(const uint32_t k, uint32_t b[])
{
	hashTable *t = (hashTable *) malloc(sizeof(hashTable));
	if (t)
	{
		t->h = (listNode **) calloc(k, sizeof(listNode *));
		t->l = k;
		t->s[0] = b[0]; t->s[1] = b[1]; // The salt that we will use
                t->s[2] = b[2]; t->s[3] = b[3]; // for this Bloom Filter.
		return t;
	}
	else   { return NIL; }
}

void delHT(hashTable *t) 
{
	for (uint32_t i = 0; i < t->l; i += 1)
	{
		delLL(t->h[i]); // Free each list
	}
	free(t->h); free(t); // Then free the container
	return;
}

listNode *findHT(hashTable *h, const char *key)
{
	uint32_t t = hash(h, key) % h->l;
	return findLL(&h->h[t], key);
}

void insertHT(hashTable *h, const char *key, const char *tran)
{
	uint32_t t = hash(h, key) % h->l;
	h->h[t] = insertLL(&h->h[t], key, tran);
	return;
}

void printHT(const hashTable *h)
{
	for (uint32_t i = 0; i < h->l; i += 1)
	{
		if (h->h[i] != NIL)
		{
			printf("h[%u]: ", i); printLL(h->h[i]);
		}
	}
	return;
}
