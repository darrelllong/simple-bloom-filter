# include <stdlib.h>
# include <stdint.h>
# include <string.h>
# include <stdio.h>
# include "ll.h"

extern bool moveToFront;
extern uint32_t distance, count;

listNode *newNode(const char *key, const char *tran)
{
	listNode *t = (listNode *) malloc(sizeof(listNode));
	if (t)
	{
		t->key  = strdup(key);
		t->tran = strdup(tran);
		t->next = NIL;
		return t;
	}
	else { return NIL; }
}

void delNode(listNode *n) { free(n->key); free(n->tran); free(n); return; }

void delLL(listNode *n)
{
	while (n != NIL)
	{
		listNode *t = n;
		n = n-> next;
		delNode(t);
	}
	return;
}

void printLL(listNode *n)
{
        for (listNode *t = n; t != NIL; t = t->next)
        {
		if (strlen(t->tran) == 0)
		{
			printf("%s\n", t->key);
		}
		else
		{
			printf("%s -> %s\n", t->key, t->tran);
		}
	}
	return;
}

listNode *findLL(listNode **n, const char *key)
{
	count += 1;
	listNode *follow = NIL;
	for (listNode *t = *n; t != NIL; t = t->next)
	{
		distance += 1;
		if (strcmp(t->key, key) == 0) 
		{
			if (moveToFront && follow != NIL)
			{
				follow->next = t->next;
				t->next = *n;
				*n = t;
			}
			return t;
		}
		follow = t;
	}
	return NIL;
} 

listNode *insertLL(listNode **n, const char *key, const char *tran)
{
	if (findLL(n, key)) 
	{
		return *n;
	}
	else
	{
		listNode *t = newNode(key, tran);
		t->next = *n;
		return t;
	}
}
