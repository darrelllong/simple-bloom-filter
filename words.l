%top{
# include <stdio.h>
int yywrap(void) { return 1; }
}

LET	[A-Za-z_]
DIG	[0-9]
WS	[ \t\r\n]
PUNC	[~`!@#$%^&*()_+-={}|\\\[\]:;\"\'<>,./?]

%%

{LET}+				{ return  0; }
{LET}+"'"({LET}|{LET}{LET})	{ return  0; }
{LET}+{DIG}+			{ return  0; }
{PUNC}+				{}
{WS}+				{}
<<EOF>>				{ return -1; }
.				{}

%%

// int main(void) { int res; while (!(res = yylex())) printf("%d - %s\n", res, yytext); }
