// bf.h — Bloom Filter
// Author: Prof. Darrell Long

# ifndef NIL
# define NIL (void *) 0
# endif
# ifndef _BF_H
# define _BF_H
# include <stdint.h>
# include <stdlib.h>
# include <stdio.h>

typedef struct bloomF {
	uint8_t *v;	// Vector
	uint32_t l;	// Length
	uint32_t s[4];	// Salt
} bloomF;

// Each function has its own hash function, determined by the salt.

uint32_t hashBF(bloomF *, char *);

// Create a new Bloom Filter of a given length and hash function.

static inline bloomF *newBF(uint32_t l, uint32_t b[])
{
	bloomF *v = (bloomF *) malloc(sizeof(bloomF));
	if (v)
	{
		v->v = (uint8_t *) calloc(l / 8 + 1, sizeof(uint8_t));
		v->l = l;
		v->s[0] = b[0]; v->s[1] = b[1]; // The salt that we will use
		v->s[2] = b[2]; v->s[3] = b[3]; // for this Bloom Filter.
		return v;
	}
	else { return NIL; }
}

// Delete a Bloom filter

static inline void delBF(bloomF *v) { free(v->v); free(v); return; }

// Return the value of position k in the Bloom filter

static inline uint32_t valBF(bloomF *x, uint32_t k)
{
	return (x->v[k >> 3] & (0x1 << (k & 7))) >> (k & 7);
}

static inline uint32_t lenBF(bloomF *x) { return x->l; }

// Count bits in the Bloom filter

static inline uint32_t countBF(bloomF *b)
{
        uint32_t c = 0;

        for (uint32_t i = 0; i < lenBF(b); i += 1)
        {
                c += valBF(b, i);
        }

        return c;
}

// Set an entry in the Bloom filter

static inline void setBF(bloomF *x, char * key) 
{
	uint32_t k = hashBF(x, key);

	x->v[k >> 3] |= (0x1 << (k & 7));
	return;
}

// Clear an entry in the Bloom filter

static inline void clrBF(bloomF *x, char *key)
{
	uint32_t k = hashBF(x, key);

	x->v[k >> 3] &= ~(0x1 << (k & 7));
	return;
}

// Check membership in the Bloom filter

static inline uint32_t memBF(bloomF *x, char *key)
{
	return valBF(x, hashBF(x, key));
}


static inline void printBF(bloomF *x)
{
	for (uint32_t i = 0; i < x->l; i += 1)
	{
		printf("%u", valBF(x, i));
	}
	printf("\n");
	return;
}

# endif
